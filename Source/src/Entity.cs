﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FantasyGame.Engine
{
    public sealed class Entity : Object
    {
        private static List<Entity> entitys = new List<Entity>();

        public string name { get; set; }

        private readonly Dictionary<Type, Component> _componentDict = new Dictionary<Type, Component>();

        private Entity(string name) {
            this.name = name;
        }

        public override void Dispose()
        {
            foreach (Type type in _componentDict.Keys) {
                this.RemoveComponent(type);
            }

            base.Dispose();
        }

        public T AddComponent<T>() where T : Component, new()
        {
            Type type = typeof(T);
            if (this._componentDict.TryGetValue(type, out Component component))
            {
                throw new Exception($"component {type.Name} already exist in {name}[{GetInstanceID()}]");
            }
            else
            {
                component = Activator.CreateInstance<T>();
                component.Entity = this;
                this._componentDict.Add(type, component);
                ObjectLooper.Instance.Add(component);
                ObjectLooper.Instance.Awake(component);
            }

            return (T)component;
        }

        public T AddComponent<T, A>(A a) where T : Component, new()
        {
            Type type = typeof(T);
            if (this._componentDict.TryGetValue(type, out Component component))
            {
                throw new Exception($"component {type.Name} already exist in {name}[{GetInstanceID()}]");
            }
            else
            {
                component = Activator.CreateInstance<T>();
                component.Entity = this;
                this._componentDict.Add(type, component);
                ObjectLooper.Instance.Add(component);
                ObjectLooper.Instance.Awake(component, a);
                
            }

            return (T)component;
        }

        public T AddComponent<T, A, B>(A a, B b) where T : Component, new()
        {
            Type type = typeof(T);
            if (this._componentDict.TryGetValue(type, out Component component))
            {
                throw new Exception($"component {type.Name} already exist in {name}[{GetInstanceID()}]");
            }
            else
            {
                component = Activator.CreateInstance<T>();
                component.Entity = this;
                this._componentDict.Add(type, component);
                ObjectLooper.Instance.Add(component);
                ObjectLooper.Instance.Awake(component, a, b);
            }

            return (T)component;
        }

        public T AddComponent<T, A, B, C>(A a, B b, C c) where T : Component, new()
        {
            Type type = typeof(T);
            if (this._componentDict.TryGetValue(type, out Component component))
            {
                throw new Exception($"component {type.Name} already exist in {name}[{GetInstanceID()}]");
            }
            else
            {
                component = Activator.CreateInstance<T>();
                component.Entity = this;
                this._componentDict.Add(type, component);
                ObjectLooper.Instance.Add(component);
                ObjectLooper.Instance.Awake(component, a, b, c);
            }

            return (T)component;
        }

        public void RemoveComponent(Type type)
        {
            if (!this._componentDict.TryGetValue(type, out Component component))
            {
                return;
            }

            ObjectLooper.Instance.Remove(component);
            this._componentDict.Remove(type);
        }

        public void RemoveComponent<T>() where T : Component
        {
            RemoveComponent(typeof(T));
        }

        public T GetComponent<T>() where T : Component
        {
            Component component;
            if (!this._componentDict.TryGetValue(typeof(T), out component))
            {
                return default(T);
            }

            return (T)component;
        }

        public Component[] GetComponents()
        {
            return this._componentDict.Values.ToArray();
        }

        public static Entity Create(string name)
        {
            Entity entity = new Entity(name);
            entitys.Add(entity);
            return entity;
        }

        public static Entity Find(string name) {
            return entitys.First((entity) => { return entity.name == name; });
        }
    }
}
