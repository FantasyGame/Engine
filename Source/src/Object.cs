﻿using FantasyGame.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyGame.Engine
{
    public abstract class Object : IDisposable
    {
        protected long _id;

        internal bool enabled { get; private set; }

        protected Object() {
            enabled = true;
            _id = IdGenerater.GenerateId();
        }

        public long GetInstanceID() {
            return _id;
        }

        public void SetEnable(bool value) {
            enabled = value;
        }

        public virtual void Dispose()
        {
            enabled = false;
            _id = 0;
        }
    }
}
