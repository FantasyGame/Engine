﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyGame.Engine
{
    public interface IOnDestroy
    {
        void OnDestroy();
    }
}
