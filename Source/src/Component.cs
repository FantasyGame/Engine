﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyGame.Engine
{
    public class Component : Object
    {
        public Entity Entity { get; set; }

        public Entity GetEntity() {
            return this.Entity;
        }

        public Component() : base() { }

        public override void Dispose()
        {
            Entity = null;
            base.Dispose();
        }
    }
}
