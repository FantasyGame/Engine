﻿using FantasyGame.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FantasyGame.Engine
{
    public interface IObjectEvent {
        Type Type();

        void Set(object v);
    }

    public abstract class ObjectEvent<T> : IObjectEvent where T : Component {
        private T value;

        protected T Get() {
            return value;
        }

        public void Set(object v) {
            this.value = (T)v;
        }

        public Type Type()
        {
            return typeof(T);
        }
    }

    public class ObjectLooper
    {
        private static ObjectLooper _instance;

        public static ObjectLooper Instance {
            get {
                return _instance = _instance ?? new ObjectLooper();
            }
        }

        private readonly Dictionary<string, Assembly> assemblies = new Dictionary<string, Assembly>();
        private Dictionary<Type, IObjectEvent> objectEvents = new Dictionary<Type, IObjectEvent>();

        private List<Component> loaders = new List<Component>();
        private List<Component> starts = new List<Component>();
        private List<Component> updates = new List<Component>();

        public void AddAssembly(string name, Assembly assembly)
        {
            this.assemblies[name] = assembly;

            this.objectEvents = new Dictionary<Type, IObjectEvent>();

            foreach (Assembly ass in assemblies.Values)
            {
                Type[] types = ass.GetTypes();
                foreach (Type type in types)
                {
                    if (!typeof(IObjectEvent).IsAssignableFrom(type) || type.IsAbstract == true || type.IsInterface == true)
                    {
                        continue;
                    }
                    object obj = Activator.CreateInstance(type);
                    IObjectEvent objectEvent = obj as IObjectEvent;
                    if (objectEvent == null)
                    {
                        Logger.Warning($"组件事件没有继承IobjectEvent: {type.Name}");
                        continue;
                    }
                    this.objectEvents[objectEvent.Type()] = objectEvent;
                }
            }

            this.Load();
        }

        public Assembly GetAssembly(string name)
        {
            return this.assemblies[name];
        }

        public Assembly[] GetAllAssembly()
        {
            return this.assemblies.Values.ToArray();
        }

        internal void Add(Component component)
        {
            if (!this.objectEvents.TryGetValue(component.GetType(), out IObjectEvent objectEvent))
            {
                return;
            }
            if (objectEvent is ILoad)
            {
                this.loaders.Add(component);
            }
            if (objectEvent is IStart)
            {
                this.starts.Add(component);
            }
            if (objectEvent is IUpdate)
            {
                this.updates.Add(component);
            }
        }

        internal void Remove(Component component) {
            this.OnDestroy(component);
            this.loaders.Remove(component);
            this.starts.Remove(component);
            this.updates.Remove(component);
        }

        internal void Awake(Component component)
        {
            if (!this.objectEvents.TryGetValue(component.GetType(), out IObjectEvent objectEvent))
            {
                return;
            }
            IAwake iAwake = objectEvent as IAwake;
            if (iAwake == null)
            {
                return;
            }
            objectEvent.Set(component);
            iAwake.Awake();
        }

        internal void Awake<T1>(Component component, T1 t1)
        {
            if (!this.objectEvents.TryGetValue(component.GetType(), out IObjectEvent objectEvent))
            {
                return;
            }
            IAwake<T1> iAwake = objectEvent as IAwake<T1>;
            if (iAwake == null)
            {
                return;
            }
            objectEvent.Set(component);
            iAwake.Awake(t1);
        }

        internal void Awake<T1, T2>(Component component, T1 t1, T2 t2)
        {
            if (!this.objectEvents.TryGetValue(component.GetType(), out IObjectEvent objectEvent))
            {
                return;
            }
            IAwake<T1, T2> iAwake = objectEvent as IAwake<T1, T2>;
            if (iAwake == null)
            {
                return;
            }
            objectEvent.Set(component);
            iAwake.Awake(t1, t2);
        }

        internal void Awake<T1, T2, T3>(Component component, T1 t1, T2 t2, T3 t3)
        {
            if (!this.objectEvents.TryGetValue(component.GetType(), out IObjectEvent objectEvent))
            {
                return;
            }
            IAwake<T1, T2, T3> iAwake = objectEvent as IAwake<T1, T2, T3>;
            if (iAwake == null)
            {
                return;
            }
            objectEvent.Set(component);
            iAwake.Awake(t1, t2, t3);
        }

        internal void Load()
        {
            for (int i = 0; i < loaders.Count; i++) {
                Component component = loaders[i];
                if (component.enabled == false) {
                    continue;
                }

                if (!objectEvents.TryGetValue(component.GetType(), out IObjectEvent objectEvent))
                {
                    continue;
                }

                ILoad iLoad = objectEvent as ILoad;
                if (iLoad == null)
                {
                    continue;
                }
                objectEvent.Set(component);

                iLoad.Load();
            }
        }

        internal void Start()
        {
            for (int i = 0; i < this.starts.Count; i++) {
                Component component = this.starts[i];
                if (!component.enabled) {
                    continue;
                }

                if (!objectEvents.TryGetValue(component.GetType(), out IObjectEvent objectEvent))
                {
                    continue;
                }

                IStart iStart = objectEvent as IStart;
                if (iStart == null)
                {
                    continue;
                }
                objectEvent.Set(component);
                iStart.Start();
            }

            this.starts.Clear();
        }

        public void Update()
        {
            this.Start();
            for (int i = 0; i < this.updates.Count; i++) {
                Component component = this.updates[i];
                if (component.enabled == false)
                {
                    continue;
                }

                if (!objectEvents.TryGetValue(component.GetType(), out IObjectEvent objectEvent))
                {
                    continue;
                }

                IUpdate iUpdate = objectEvent as IUpdate;
                if (iUpdate == null)
                {
                    continue;
                }
                objectEvent.Set(component);

                iUpdate.Update();
            }
        }

        internal void OnDestroy(Component component)
        {
            if (!this.objectEvents.TryGetValue(component.GetType(), out IObjectEvent objectEvent))
            {
                return;
            }
            IOnDestroy iOnDestroy = objectEvent as IOnDestroy;
            if (iOnDestroy == null)
            {
                return;
            }
            objectEvent.Set(component);
            iOnDestroy.OnDestroy();
            component.Dispose();
        }

    }
}
